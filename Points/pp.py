import numpy as np
import sys
import os.path as op

shall=True
addshall=True
if len(sys.argv)>2:
  if sys.argv[2]=="no":
    shall=False
    addshall=False
who=""
if shall==False:
  who=sys.argv[1]
which=""
howmuch=""
howmax=""
if shall:
  if len(sys.argv)>1:
    who=sys.argv[1]
  if len(who)==0:who=input("Welche Gruppe?")
  if len(sys.argv)>2:
    which=sys.argv[2]
  if len(which)==0:which=input("Welche Übung?")
  if len(sys.argv)>3:
    howmuch=sys.argv[3]
  if len(howmuch)==0:howmuch=input("Wie viele Punkte hast du?")
  if len(sys.argv)>4:
    howmax=sys.argv[4]
  if len(howmax)==0:howmax=input("Wie viele Punkte hättest du haben können?")


lwhich=[]
lmuch=[]
lmax=[]
if op.isfile(who+".npz"):
  thefil=np.load(who+".npz")
  lwhich=thefil["which"].tolist()
  lmuch=thefil["much"].tolist()
  lmax=thefil["max"].tolist()
  
if addshall:
  lwhich.append(which)
  lmuch.append(float(howmuch))
  lmax.append(float(howmax))
  np.savez_compressed(who,which=lwhich,much=lmuch,max=lmax)

tc=len(lmax)
tmuch=0.0
tmax=0.0
for i in range(tc):
  tmuch+=lmuch[i]
  tmax+=lmax[i]
  print("Exercise",(i+1),"::",lmuch[i],"of",lmax[i])
part=tmuch/tmax
print("Found ",tc,"Exercises for group",who)
print("You got",tmuch,"out of",tmax,"Points")
print("This is equal to ",str(round(part*100,2))+"%","of all Points")
if part>=0.5:
  print("Your all good")
  if part<0.7:
    print("(But it migth be a little tigth)")
  if part>0.9:
    print("(actually your really good)")
else:
  print("Thats not that good...")