import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append("Scripts/")
from Praktikum import *
#Konstanten
R=8.3144621
G=6.67408e-11
colors=np.array(['g','b','y','k','r'])
names=np.array([r'H',r'$H_2$',r'He',r'$O_2$',r'$CO_2$'])
masses=np.array([1,2,4,32,44]) # in gr/mol

def velocity(T,m):
	return(np.sqrt(8*R*T/np.pi/m/1e-3))  #maxwell boltzmann mean thermal velocity einfügen als funktion von T und m[gr/mol]

def escape(m,r):
	return(np.sqrt(2*G*m/r))

T=np.linspace(0,1000,1000)
M=np.linspace(1,50,1000)
plt.figure()
for i in range(len(names)):
	plt.plot(T,velocity(T,masses[i]),label=names[i],color=colors[i])
txy('Mean velocities depending on the Temperature','Temperature in K',r'Velocity in $\frac{m}{s}$ ')
plt.legend()
plt.savefig('velocity_temperature.pdf',format='pdf')

planets=np.array([r'Earth',r'Mars',r'Jupiter',r'Uranus'])
planet_mass=np.array([5.972e24,6.4171e23,1.8982e27,8.6810e25])
planet_radius=np.array([6371,3389.5,69911,25362])*1e3
escapes=escape(planet_mass,planet_radius)*1e-3

temperatures=np.array([288,210,165,76])

fig,axe=plt.subplots(2,2,sharex=True,sharey=True,figsize=[9,9])
for i in range(2):
	for j in range(2):
		axe[i,j].set_title(planets[i+j*2])
		axe[i,j].plot(M,velocity(temperatures[i+j*2],M)*1e-3,label='Mean thermal velocity for the planet')
		axe[i,j].axhline(escapes[i+j*2],lw=5,color='green',label='Escape velocity for the planet')
		axe[i,j].text(18,escapes[i+j*2]-4,str(np.round(escapes[i+j*2],0))+r'  $\frac{m}{s}$')

		#for k in range(5):
			#axe[i,j].axvline(masses[k],color=colors[k])
txy('Mean Velocity depending on the molecular mass \n \n',r'Molecular Mass in $\frac{g}{mol}$',r'Velocity in $\frac{km}{s}$',figure=fig)
axe[0,0].legend()
fig.savefig('velocity_mass.pdf',format='pdf')

