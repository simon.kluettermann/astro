# -*- coding: utf-8 -*-
"""
Created on Wed Mar  5 18:03:48 2014

Useful tools for Grundpraktikum Physik, based on MAPLE sheet Praktikum.mws

@author:Henning 'Drehhenning' Gast
@coauthor: seron (python 3.5 implementierung, trafo und Tex_Table)
"""

import numpy as np
from numpy import sqrt,sin,cos,log,exp
import scipy
import scipy.fftpack
import scipy.odr
from scipy.misc import derivative
import io
import matplotlib.pyplot as plt
from scipy.optimize import leastsq
import matplotlib.patches as patches
import matplotlib.mlab as mlab
import scipy.stats as scis


def lese_lab_datei(dateiname):
    '''
    CASSY LAB Datei einlesen.

    Messdaten werden anhand von Tabulatoren identifiziert.

    Gibt ein numpy-Array zurueck.

    '''
    f = open(dateiname)
    dataSectionStarted = False
    dataSectionEnded = False
    data = ''
    for line in f:
        if '\t' in line and not dataSectionEnded:
            data += line
            dataSectionStarted = True
        if not '\t' in line and dataSectionStarted:
            dataSectionEnded = True
    f.close()
    return np.genfromtxt(io.BytesIO(data.encode()))


def lineare_regression(x,y,ey):
    '''

    Lineare Regression.

    Parameters
    ----------
    x : array_like
        x-Werte der Datenpunkte
    y : array_like
        y-Werte der Datenpunkte
    ey : array_like
        Fehler auf die y-Werte der Datenpunkte

    Diese Funktion benoetigt als Argumente drei Listen:
    x-Werte, y-Werte sowie eine mit den Fehlern der y-Werte.
    Sie fittet eine Gerade an die Werte und gibt die
    Steigung a und y-Achsenverschiebung b mit Fehlern
    sowie das chi^2 und die Korrelation von a und b
    als Liste aus in der Reihenfolge
    [a, ea, b, eb, chiq, cov].
    '''

    s   = sum(1./ey**2)
    sx  = sum(x/ey**2)
    sy  = sum(y/ey**2)
    sxx = sum(x**2/ey**2)
    sxy = sum(x*y/ey**2)
    delta = s*sxx-sx*sx
    b   = (sxx*sy-sx*sxy)/delta
    a   = (s*sxy-sx*sy)/delta
    eb  = sqrt(sxx/delta)
    ea  = sqrt(s/delta)
    cov = -sx/delta
    corr = cov/(ea*eb)
    chiq  = sum(((y-(a*x+b))/ey)**2)

    return(a,ea,b,eb,chiq,corr)


def lineare_regression_xy(x,y,ex,ey):
    '''

    Lineare Regression mit Fehlern in x und y.

    Parameters
    ----------
    x : array_like
        x-Werte der Datenpunkte
    y : array_like
        y-Werte der Datenpunkte
    ex : array_like
        Fehler auf die x-Werte der Datenpunkte
    ey : array_like
        Fehler auf die y-Werte der Datenpunkte

    Diese Funktion benoetigt als Argumente vier Listen:
    x-Werte, y-Werte sowie jeweils eine mit den Fehlern der x-
    und y-Werte.
    Sie fittet eine Gerade an die Werte und gibt die
    Steigung a und y-Achsenverschiebung b mit Fehlern
    sowie das chi^2 und die Korrelation von a und b
    als Liste aus in der Reihenfolge
    [a, ea, b, eb, chiq, cov].

    Die Funktion verwendet den ODR-Algorithmus von scipy.
    '''
    a_ini,ea_ini,b_ini,eb_ini,chiq_ini,corr_ini = lineare_regression(x,y,ey)

    def f(B, x):
        return B[0]*x + B[1]

    model  = scipy.odr.Model(f)
    data   = scipy.odr.RealData(x, y, sx=ex, sy=ey)
    odr    = scipy.odr.ODR(data, model, beta0=[a_ini, b_ini])
    output = odr.run()
    ndof = len(x)-2
    chiq = output.res_var*ndof
    corr = output.cov_beta[0,1]/np.sqrt(output.cov_beta[0,0]*output.cov_beta[1,1])

    return output.beta[0],output.sd_beta[0],output.beta[1],output.sd_beta[1],chiq,corr


def fourier(t,y):
    '''

    Fourier-Transformation.

    Parameters
    ----------
    t : array_like
        Zeitwerte der Datenpunkte
    y : array_like
        y-Werte der Datenpunkte

    Gibt das Fourierspektrum in Form zweier Listen (freq,amp)
    zurueck, die die Fourieramplituden als Funktion der zugehoerigen
    Frequenzen enthalten.
    '''

    dt = (t[-1]-t[0])/(len(t)-1)
    fmax = 0.5/dt
    step = fmax/len(t)
    freq=np.arange(0.,fmax,2.*step)
    amp = np.zeros(len(freq))
    i=0
    for f in freq:
        omega=2.*np.pi*f
        sc = sum(y*cos(omega*t))/len(t)
        ss = sum(y*sin(omega*t))/len(t)
        amp[i] = sqrt(sc**2+ss**2)
        i+=1
    return (freq,amp)


def fourier_fft(t,y):
    '''

    Schnelle Fourier-Transformation.

    Parameters
    ----------
    t : array_like
        Zeitwerte der Datenpunkte
    y : array_like
        y-Werte der Datenpunkte

    Gibt das Fourierspektrum in Form zweier Listen (freq,amp)
    zurueck, die die Fourieramplituden als Funktion der zugehoerigen
    Frequenzen enthalten.
    '''
    dt = (t[-1]-t[0])/(len(t)-1)
    amp = abs(scipy.fftpack.fft(y))
    freq = scipy.fftpack.fftfreq(t.size,dt)
    return (freq,amp)


def exp_einhuellende(t,y,ey,Sens=0.1):
    '''
    Exponentielle Einhuellende.

    Parameters
    ----------
    t : array_like
        Zeitwerte der Datenpunkte
    y : array_like
        y-Werte der Datenpunkte
    ey : array_like
        Fehler auf die y-Werte der Datenpunkte
    Sens : float, optional
        Sensitivitaet, Wert zwischen 0 und 1

    Die Funktion gibt auf der Basis der drei Argumente (Listen
    mit t- bzw. dazugehoerigen y-Werten plus y-Fehler) der Kurve die
    Parameter A0 und delta samt Fehlern der Einhuellenden von der Form
    A=A0*exp(-delta*t) (Abfallende Exponentialfunktion) als Liste
    [A0, sigmaA0, delta, sigmaDelta] aus.
    Optional kann eine Sensibilitaet angegeben werden, die bestimmt,
    bis zu welchem Prozentsatz des hoechsten Peaks der Kurve
    noch Peaks fuer die Berechnung beruecksichtigt werden (default=10%).
    '''
    if not 0.<Sens<1.:
        raise ValueError('Sensibilitaet muss zwischen 0 und 1 liegen!')

    # Erstelle Liste mit ALLEN Peaks der Kurve
    Peaks=[]
    PeakZeiten=[]
    PeakFehler=[]
    GutePeaks=[]
    GutePeakZeiten=[]
    GutePeakFehler=[]
    if y[0]>y[1]:
        Peaks.append(y[0])
        PeakZeiten.append(t[0])
        PeakFehler.append(ey[0])
    for i in range(1,len(t)-1):
        if y[i] >= y[i+1] and \
           y[i] >= y[i-1] and \
           ( len(Peaks)==0 or y[i] != Peaks[-1] ): #handle case "plateau on top of peak"
           Peaks.append(y[i])
           PeakZeiten.append(t[i])
           PeakFehler.append(ey[i])

    # Loesche alle Elemente die unter der Sensibilitaetsschwelle liegen
    Schwelle=max(Peaks)*Sens
    for i in range(0,len(Peaks)):
        if Peaks[i] > Schwelle:
            GutePeaks.append(Peaks[i])
            GutePeakZeiten.append(PeakZeiten[i])
            GutePeakFehler.append(PeakFehler[i])

    # Transformiere das Problem in ein lineares
    PeaksLogarithmiert = log(np.array(GutePeaks))
    FortgepflanzteFehler = np.array(GutePeakFehler) / np.array(GutePeaks)
    LR = lineare_regression(np.array(GutePeakZeiten),PeaksLogarithmiert,FortgepflanzteFehler)

    A0=exp(LR[2])
    sigmaA0=LR[3]*exp(LR[2])
    delta=-LR[0]
    sigmaDelta=LR[1]
    return(A0,sigmaA0,delta,sigmaDelta)

def untermenge_daten(x,y,x0,x1):
    '''
    Extrahiere kleinere Datensaetze aus (x,y), so dass x0 <= x <= x1
    '''
    xn=[]
    yn=[]
    for i,v in enumerate(x):
        if x0<=v<=x1:
            xn.append(x[i])
            yn.append(y[i])

    return (np.array(xn),np.array(yn))

def peak(x,y,x0,x1):
    '''
    Approximiere ein lokales Maximum in den Daten (x,y) zwischen x0 und x1.
    '''
    N = len(x)
    ymin = max(y)
    ymax = min(y)
    i1 = 0
    i2 = N-1
    for i in range(N):
       if x[i]>=x0:
         i1=i
         break
    for i in range(N):
       if x[i]>=x1:
         i2=i+1
         break
    for i in range(i1,i2):
      if y[i]>ymax:
          ymax=y[i]
      if y[i]<ymin:
          ymin=y[i]

    sum_y   = sum(y[i1:i2])
    sum_xy  = sum(x[i1:i2]*y[i1:i2])
    xm = sum_xy/sum_y
    return xm

def peakfinder_schwerpunkt(x,y):
    '''
    Finde Peak in den Daten (x,y).
    '''
    N = len(x)
    val = 1./sqrt(2.)
    i0=0
    i1=N-1
    ymax=max(y)
    for i in range(N):
        if y[i]>ymax*val:
            i0=i
            break
    for i in range(i0+1,N):
        if y[i]<ymax*val:
            i1=i
            break
    xpeak = peak(x,y,x[i0],x[i1])
    return xpeak


def gewichtetes_mittel(y,ey):
    '''
    Berechnet den gewichteten Mittelwert der gegebenen Daten.

    Parameters
    ----------
    y : array_like
        Datenpunkte
    ey : array_like
        Zugehoerige Messunsicherheiten.

    Gibt den gewichteten Mittelwert samt Fehler zurueck.
    '''
    w = 1/ey**2
    s = sum(w*y)
    wsum = sum(w)
    xm = s/wsum
    sx = sqrt(1./wsum)

    return (xm,sx)


def trafo(a,f_x,df_x=False):
    '''
    Transformiert array in gewünschte Funktion.
    
    Parameter
    ---------
    a : array_like
        x-Datenpunkte
    f_x : string
        Funktion von x als string (z.B. np.log(x))
    df_x : boolean
        legt fest, ob ebenfalls die numerische Ableitung ausgegeben werden soll
    '''
    f= lambda x : eval(f_x)
    x_array=np.zeros(len(a))
    df_array=np.zeros(len(a))
    for i in range(len(a)):
        x_array[i]=f(a[i])
        if df_x == True:
            df_array[i]=derivative(f,x_array[i],dx=1e-6)
    if df_x == False:
        return(x_array)
    else:
        return(x_array,df_array)
    
def tex_table(array):
    '''
    Transformiert numpy.array in fertigen Tex-Table
    
    Parameter
    ---------
    array: Numpy array
    
    Gibt fertigen TeX-Tabelle als string aus.
    (Überschriften und Captions müssen immer noch händisch vorgenommen werden)
    '''
    length=array.shape[0]
    width=array.shape[1]
    string=r'\begin{table}[H]'+'\n'
    string+=r'\centering'+'\n'
    string+=r'\begin{tabular}'+'{'
    for i in range(width):
        string+='|c'
    string+='|} \n'
    string+='\hline \n'
    substring=r''
    for i in range(width-1):
        substring+='&'
    string+=substring + r'\\' +'\n' +'\hline \n'
    substring=r''
    for i in range(length):
        string+='\hline \n'
        for k in range(width-1):
            substring+=str(array[i,k])+r'&'
        substring+=str(array[i,width-1])+r'\\'+'\n'
        string+=substring
        substring=r''
    string+='\hline \n'
    string+=r'\end{tabular}' +'\n'
    string+=r'\caption{}'+'\n'
    string+=r'\end{table}' +'\n'
    return(string)

def regression_plotter(x,y,xerr,yerr,f_x,Y_axis,X_axis,Title,savepath=0,form=0,show=True,params=2):
    '''
    x : array-like
        x-Daten
    y : array-like
        y-Daten
    xerr : array-like
        x-Fehler
    yerr : array-like
        y-Fehler
    f_x : string
          angepasste Funktion als string
    Y_axis : (raw)-string
             Y-Achsenbeschriftung
    X_axis : (raw)-string
             X-Achsenbeschriftung
    Title : (raw)-string
            Überschrift
    savepath : datei pfad als string wo, wenn gegeben die figure gespeichert wird
    form : im normalfalll gibt es nur pdf bilder aber mit form können auch png ausgeben werden einfach form = 'png' eingeben 
    close : fals True werden alle plots am Ende der funktion wieder geschlossen
    Plottet Zweidimensionale Regression mit Residuum
    '''
    if str(type(params))!="<class 'int'>":
        ddof=len(params)
        f_x=f_x.format(*params)
    else:
        ddof=2
    if str(type(f_x))=="<class 'str'>":
        X=np.array(x)
        Y=np.array(y)
        Xerr=np.array(xerr)
        Yerr=np.array(yerr)
        
        #Berechnen der Nötigen Güten, Kenngrößen, Fehler etc.
        f_X=trafo(X,f_x,True)[0]
        f_dash_x= trafo(X,f_x,True)[1]
        ges_err=np.sqrt(Yerr**2 +f_dash_x**2*Xerr**2)
        Y_dash=Y-f_X
        chiq=np.round(sum(Y_dash**2/ges_err**2),1)
            #chiq=np.round(np.var(Y_dash/ges_err,ddof=2)*(len(Y_dash)-1),2)
        conf=np.round((1-scis.chi2.cdf(chiq,len(Y_dash)-ddof))*100,1)
        weights = np.ones_like(Y_dash)/(len(Y_dash)-ddof)
        
        
        
        #Aufhübschung der Parameter und einsetzen in die labels
        k = 2
        pparams=np.round(params, k)
        while any(pparams)==0:
            pparams=np.round(params,k+1)
            k=k+1
        labels=[]
        labels.append(r'$F_{'+str(len(Y_dash)-ddof)+'}$'+'({0})={1}%'.format(chiq,conf))
        for i in range(len(params)):
            labels.append('p[{0}]={1}'.format(i,pparams[i]))
            
        labels.append(r'$\frac{\chi^2}{ndof}=$'+'{0:g}/{1:g}'.format(chiq,(len(Y_dash)-ddof)))
        
        
        #Binning
        bins=np.linspace(np.round(min(Y_dash/ges_err))-0.5,np.round(max(Y_dash/ges_err))+0.5,np.round(max(Y_dash/ges_err))-np.round(min(Y_dash/ges_err))+2)
        #Plotten
        fig1,(ax1,ax2) = plt.subplots(2,sharex=True,gridspec_kw={'height_ratios':[4,1]})

        ax1.errorbar(X,Y,xerr=Xerr,yerr=Yerr,fmt='.',ecolor='#A9BCF5',label=r'$F_{'+str(len(Y_dash)-ddof)+'}$'+'({0})={1}%'.format(chiq,conf))

        ax1.plot(X,f_X,c='r',label=r'$\frac{\chi^2}{ndof}=$'+'{0:g}/{1:g}'.format(chiq,(len(Y_dash)-ddof)))
        ax1.set_title(Title,y=1.13)
        ax1.set_ylabel(Y_axis)
        ax1.grid(True)
        ax2.set_title('Residual')
        ax2.set_xlabel(X_axis)
        ax2.set_ylabel(Y_axis +'- fit')
        ax2.errorbar(X,Y_dash,yerr=ges_err,fmt='.',capsize=3,ecolor='#A9BCF5')
        ax2.grid(True)
        for i in params:
            ax1.plot(X[0],Y[0],alpha=1)
            
        ax1.legend(labels,bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
           ncol=2+len(pparams), mode="expand", borderaxespad=0.,frameon=True,handlelength=0,handleheight=0)
        if savepath !=0 and form == 0:
            plt.savefig(savepath+'_reg.pdf',format='pdf')
        if savepath !=0 and form != 0:
            plt.savefig(savepath+'_reg.'+form,format=form)
        fig2,ax = plt.subplots()

        ax.hist(Y_dash/ges_err,bins=bins,histtype='stepfilled',weights=weights,alpha=0.5)

        ax.plot(np.linspace(-3,3,100), mlab.normpdf(np.linspace(-3,3,100),0,1), color='r')
        ax.set_title('Pullverteilung')
        ax.set_xlabel('Pull')
        ax.set_ylabel('%')
    
        if savepath !=0 and form == 0:
            plt.savefig(savepath+'_pullhist.pdf',format='pdf')
        if savepath !=0 and form != 0:
            plt.savefig(savepath+'_pullhist.'+form,format=form)
        if show == False:
            plt.close()
    elif str(type(f_x))!="<class 'str'>":
        print()
        print(str(type(f_x)))
        print("Hey du, ja genau du! Ditt wird so nix. Gib mir die Funktion mal lieber irgendwie so: '{0}*x+{1}'.format(a,b)', ditt mag ich lieber.")
#Funktion,start Parameter,x Werte, Y Werte, Y Fehler
#error_list=1 heißt keine Fehler
def gauss(x,p):
    val=p[0]+p[3]*np.sqrt(1.0/2.0/np.pi/p[2]**2)*np.exp(-((x-p[1])**2/2.0/p[2]**2)) 
    return(val)

def fit(func,start,x_list,y_list,error_list,x_error=0,derv=0):
	if np.size(x_error)==1:
		fPull = lambda p,x,y,ey: (y-func(x,p))/ey
		par_f, cov_f, info, errmsg, ierr =leastsq(fPull, start, args=(x_list,y_list,error_list), full_output=True, maxfev=10000);
	else:
		if derv== 0:
			print("Du musst eine Steigungsfunktion angeben!")
		fPull = lambda p,x,y,ey,ex: (y-func(x,p))/(ey**2+derv(x,p)**2*ex**2)**(0.5)
		par_f, cov_f, info, errmsg, ierr =leastsq(fPull, start, args=(x_list,y_list,error_list,x_error), full_output=True, maxfev=10000);	
	if ierr not in [1,2,3,4]:
		print("Fehler beim Fitten")
		exit();
#	print("Convergenz nach: ",info['nfev']," Schritten.");
	chi2=(info['fvec']**2).sum();
	freiheit=len(x_list)-len(start);
	gute=chi2/freiheit;
#	print(gute,"=",chi2,"/",freiheit)
	return par_f, chi2, gute, cov_f, info['fvec']
#Parameter, chi2, gute


def derv_ps(x,p):
	return(1)
plt.close('all')

def peak_selector(data,peakzahl=1,show_plot=False,parted=False):
    if peakzahl==1 and show_plot==True :
        show_plot=False
        print('1 peak und plots anzeigen geht nicht weil subplots beuntzt wurden, aber mit zwei peaks angeben sollte es funktionieren')
    peak_indexes=[]	
    fig,axe=plt.subplots(peakzahl)
    x=np.arange(len(data))
    kleinstes=min(np.abs(data))
    for i in range(peakzahl):
        if peakzahl==1:
            j=0
        else:
            j=i    
            
        x_err=np.ones(len(data))
        e=np.ones(len(data))
        para0=(np.mean(data),np.where(data==max(data))[0][0],1,max(data))
        #print('startparameter',para0)
        para1=fit(gauss,para0,x,data,e,x_error=x_err,derv= derv_ps)
        if show_plot==True:
            axe[j].plot(x,data)
            axe[j].plot(x,gauss(x,para1[0]))
            
        #peak_indexes.append(np.where(data==max(data))[0][0])
        peak_indexes.append(int(para1[0][1]+0.5))
        if parted!=False:
            for k in range(len(data)):
                if k>peak_indexes[i]-para1[0][2]*parted and k<peak_indexes[i]+para1[0][2]*parted:
                    data[k]=kleinstes

        else:
             data=data-gauss(x,para1[0])
        if show_plot==True:
            axe[j].plot(x,data)
    if show_plot==True:
        plt.show()
    plt.close('all')
    return(peak_indexes)


def txy(title,xlabel,ylabel,figure=0):
    if figure==0:
        plt.title(title)
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
    else:
        big_ax = figure.add_subplot(111, frameon=False)
        big_ax.set_facecolor('none')
        big_ax.set_ylabel(ylabel)
        big_ax.set_xlabel(xlabel)
        big_ax.set_title(title)
        big_ax.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')

		
