import numpy as np
from numpy import sin,cos,arccos,tan

hour=15
minu=15/60
anno=365*24*3600
AU=149597870700
pc=206265*AU
#pc= 3.0857e16
demi=1/60
desec=1/3600
dera=2*np.pi/360

de1=dera*(15+52*demi)
de2=dera*(5+59*demi)
al1=dera*(4*hour+27*minu)
al2=dera*(6*hour+31*minu)
propermotion=0.12*desec/anno*dera
print('no1')
psi=arccos(sin(de1)*sin(de2)+cos(de1)*cos(de2)*cos(al1-al2))
print('psi=',psi/dera)
print('distance=',40000*tan(psi)/propermotion/pc,'parsec' )
print('--------------------------------------')


t1=1e-8
e=1.602176487e-19
h_bar=1.054571628e-34
h=6.62606896e-34
m_p=1.672621637e-27
T=5800
k=1.3806504e-23
lam=656.3e-9
rho=1e-4
n=rho/m_p
sigma=(1e-10)**2*np.pi
c=299792458
print('no3')
print('a')
print('Gamma=',h_bar/t1/e,'eV')
print('b')
v=np.sqrt(8*k*T/np.pi/m_p)
print('v=',v)
print('gamma_doppler=',h*c/lam/(c/v+1)*2/e,'eV')
print('c')
print('gamma_pressure=',h_bar/(1/np.sqrt(2)/n/sigma/v)/e,'eV' )
print('-------------------------------------')


m1=1.36
TS=5800
MS=4.8
RS=6.957e8
R1=3.28*RS
T1=12400
sig=5.6704e-8  #boltzmann konstante
#AU=1.49e11
#pc=206000*AU

print('no4')
print('a')
print('T_effektiv from wikipedia= ' ,T1)
print('b')
print('L=', T1**4 *4*np.pi*sig*R1**2)
M1=MS+2.5*np.log10(TS**4/3.28**2/T1**4)
print('Magnitude=',M1)
print('c')
print('distance=',10**(1+(m1-M1)/5),' pc')
print('-------------------------------------')

G=6.67428e-11

Ti=50*anno
mi=1/60
sec=1/3600
a=7.5*sec
d=0.379*sec
ep=0.484


print('no5')
print('a')
MT=AU**3*(a/d)**3/Ti**2*4*np.pi**2/G
print('total mass',MT)
print('c')
a1=a*(ep/(1+ep))
a2=a/(1+ep)
print('a1=',a1/sec,'\'\'')
print('a2=',a2/sec,'\'\'')
print('d')
print('m1=',MT/(1+a1/a2))
print('m2=',MT/(1+a2/a1))
print('-------------------------------------')





