# -*- coding: utf-8 -*-
"""
Created on Wed Dec  5 15:57:41 2018

@author: User
"""

import numpy as np
import matplotlib.pyplot as plt

U = np.array([5.9,9.3,13.1,14.8,17,20.6])
B = np.array([6.9,9.8,13.1,14.7,16.8,19.5])
V = np.array([7.2,9.9,13,14.4,16.1,18.3])

plt.scatter(B-V, V)
plt.text(B[0]-V[0], V[0]-1, '1')
plt.text(B[1]-V[1], V[1]-1, '2')
plt.text(B[2]-V[2], V[2]-1, '3')
plt.text(B[3]-V[3], V[3]-1, '4')
plt.text(B[4]-V[4], V[4]-1, '5')
plt.text(B[5]-V[5], V[5]-1, '6')
plt.ylim(20,-5)
plt.xlim(-0.4,1.6)
plt.xlabel('B-V')
plt.ylabel('Absolute Magnetude M_V')
plt.title('Color-Magnetude-Diagram')
plt.savefig('color_index.png', dpi=300)
plt.show()
plt.close()

plt.scatter(B-V, U-B)
plt.text(B[0]-V[0], U[0]-B[0]-0.1, '1')
plt.text(B[1]-V[1], U[1]-B[1]-0.1, '2')
plt.text(B[2]-V[2], U[2]-B[2]-0.1, '3')
plt.text(B[3]-V[3], U[3]-B[3]-0.1, '4')
plt.text(B[4]-V[4], U[4]-B[4]-0.1, '5')
plt.text(B[5]-V[5], U[5]-B[5]-0.1, '6')
plt.ylim(1.5,-1.5)
plt.xlabel('B-V')
plt.ylabel('U-B')
plt.title('Spectral Classes')
plt.savefig('spectral_class.png', dpi=300)
plt.show()
plt.close()