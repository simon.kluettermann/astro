import numpy as np
import matplotlib.pyplot as plt

from astropy import units as u
from astropy.coordinates import SkyCoord

disk= np.random.multivariate_normal(mean=[0,0], cov=np.diag([1,0.001]), size=5000)
print (disk)

plt.figure(figsize=(10,8))

ax=plt.subplot(111, projection='aitoff')
ax.set_title('Stars in galactic plane in galactic coordinates')
plt.grid(True)
ll=disk[:,0]
bb=disk[:,1]

ax.plot(ll,bb, 'o', markersize=2, alpha=0.3)
plt.savefig('Galactic.png',dpi=300)
plt.close()

diskG = np.zeros([5000,2])

diskG=SkyCoord(ll, bb, unit='rad', frame='galactic')
ra=diskG.icrs.ra.wrap_at(180 * u.deg).radian
dec=diskG.icrs.dec.radian

plt.figure(figsize=(10,8))

ax=plt.subplot(111, projection='aitoff')
ax.set_title('Stars in galactic plane in equatorial coordinates')
plt.grid(True)
ax.plot(ra,dec, 'o', markersize=2, alpha=0.3)
plt.savefig('Equatorial.png',dpi=300)
plt.close()