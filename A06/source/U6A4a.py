from astropy import units as u
from astropy.coordinates import SkyCoord

crab_nebula = SkyCoord('05:34:0', '+22°00', unit=(u.hourangle, u.deg))
andromeda = SkyCoord('00:42:0', '+41:16', unit=(u.hourangle, u.deg))
great_attractor = SkyCoord('16:15:0', '-60:54', unit=(u.hourangle, u.deg))

print('crab nebula: ', crab_nebula.galactic.l.hourangle, crab_nebula.galactic.b.deg)
print('Andromeda: ', andromeda.galactic.l.hourangle, andromeda.galactic.b.deg)
print('great attractor: ', great_attractor.galactic.l.hourangle, great_attractor.galactic.b.deg)