import numpy as np
import matplotlib.pyplot as plt
from sympy import *
import scipy.optimize as so
import sys

def Sp(x):
  return S(factor(S(expand(S(x)))))


alpha=Symbol("alpha")
r1=Matrix([[1,0,0],[0,cos(alpha),-sin(alpha)],[0,sin(alpha),cos(alpha)]])
r2=Matrix([[cos(alpha),0,sin(alpha)],[0,1,0],[-sin(alpha),0,cos(alpha)]])
r3=Matrix([[cos(alpha),-sin(alpha),0],[sin(alpha),cos(alpha),0],[0,0,1]])


phi,theta=symbols("phi theta")
r=Matrix([sin(theta)*cos(phi),sin(theta)*sin(phi),cos(theta)]).T

r=r.replace(phi,-pi/2+phi)
r=r.replace(theta,-pi/2+theta)

l,b=symbols("l b")

deltaNGP=Symbol("delta_NGP")
lNCP=Symbol("l_NCP")
alphaNGP=Symbol("alpha_NGP")

e1,e2,e3=symbols("e1 e2 e3")

e1=pi/2-deltaNGP
e3=-lNCP
e2=0#already implemented for easier trig. math



v1=r.replace(phi,l).replace(theta,b).T

v2=(r1.replace(alpha,e1))*(r3.replace(alpha,e3))*(r2.replace(alpha,e2))*v1

v2=Sp(trigsimp(Sp(v2)))


alpha,delta=symbols("alpha delta")
soll=r.replace(phi,alpha-alphaNGP).replace(theta,delta).T

for i in range(3):
  pprint(v2[i])
  print("=")
  pprint(soll[i])
  if i==2:continue
  print("")
  print("")
  print("")  
  