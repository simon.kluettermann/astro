import numpy as np
import matplotlib.pyplot as plt
from sympy import *
import scipy.optimize as so
import sys
import subprocess


def Sp(x):
  return S(factor(S(expand(S(x)))))



i = Matrix([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]])
g = Matrix([[1,0,0,0],[0,-1,0,0],[0,0,-1,0],[0,0,0,-1]])
g0 = Matrix([[0,0,1,0],[0,0,0,1],[1,0,0,0],[0,1,0,0]])
g1 = Matrix([[0,0,0,1],[0,0,1,0],[0,-1,0,0],[-1,0,0,0]])
g2 = Matrix([[0,0,0,-I],[0,0,I,0],[0,I,0,0],[-I,0,0,0]])
g3 = Matrix([[0,0,1,0],[0,0,0,-1],[-1,0,0,0],[0,1,0,0]])
k1 = Matrix([[0,1,0,0],[1,0,0,0],[0,0,0,0],[0,0,0,0]])
k2 = Matrix([[0,0,1,0],[0,0,0,0],[1,0,0,0],[0,0,0,0]])
k3 = Matrix([[0,0,0,1],[0,0,0,0],[0,0,0,0],[1,0,0,0]])
j1 = Matrix([[0,0,0,0],[0,0,0,0],[0,0,0,-1],[0,0,1,0]])
j2 = Matrix([[0,0,0,0],[0,0,0,1],[0,0,0,0],[0,-1,0,0]])
j3 = Matrix([[0,0,0,0],[0,0,-1,0],[0,1,0,0],[0,0,0,0]])

alpha=Symbol("alpha")
r1=Matrix([[1,0,0],[0,cos(alpha),-sin(alpha)],[0,sin(alpha),cos(alpha)]])
r2=Matrix([[cos(alpha),0,sin(alpha)],[0,1,0],[-sin(alpha),0,cos(alpha)]])
r3=Matrix([[cos(alpha),-sin(alpha),0],[sin(alpha),cos(alpha),0],[0,0,1]])


phi,theta=symbols("phi theta")
r=Matrix([sin(theta)*cos(phi),sin(theta)*sin(phi),cos(theta)]).T

r=r.replace(phi,-pi/2+phi)
r=r.replace(theta,-pi/2+theta)

l,b=symbols("l b")

deltaNGP=Symbol("delta_NGP")
lNCP=Symbol("l_NCP")
alphaNGP=Symbol("alpha_NGP")

e1,e2,e3=symbols("e1 e2 e3")

e1=pi/2-deltaNGP
e3=-lNCP
e2=0#already implemented for easier trig. math



v1=r.replace(phi,l).replace(theta,b).T

v2=(r1.replace(alpha,e1))*(r3.replace(alpha,e3))*(r2.replace(alpha,e2))*v1

v2=Sp(trigsimp(Sp(v2)))


alpha,delta=symbols("alpha delta")
soll=r.replace(phi,alpha-alphaNGP).replace(theta,delta).T



filename = "output"
documentString = r'''\documentclass[landscape]{article}
\usepackage[utf8]{inputenc}
\usepackage{physics}
\usepackage[landscape]{geometry}
\usepackage{amsmath}
\begin{document}
    \scrollmode
'''

for i in range(3):
  documentString+=r"\begin{align}"+latex(v2[i])+"="+latex(soll[i])+"\end{align}"


documentString += r'''
\batchmode
\end{document}'''
filename="doc"
with open(filename + ".tex", "w") as tex_file:
    tex_file.write(documentString)

print("\n\nCreate LaTeX output...")
subprocess.call(["pdflatex", "-interaction=batchmode", filename + ".tex"])
print("Output was created as " + filename + ".pdf")

